/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package erd_test;

/**
 *
 * @author lmangoua
 * Date: 19/09/2016
 */
public class Student {
    
    private String rollNo;
    private String name;
    
    //Default constructor
    public Student() {
        
    }
    
    //Constructor
    public Student(String rollNo, String name) {
        this.rollNo = rollNo;
        this.name = name;
    }
    
    //Getters & Setters
    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
